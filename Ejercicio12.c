#include <stdio.h>

//12. Write a program that asks for 10 numbers to the user and show the sum and product of all of them. Use while loop

int main ()
{
    int temp;
    int sum = 0;
    int product = 1;
    int num = 1;
    
    printf("Type ten numbers(inegers):");
    
    while (num <=10)
    {
        num++;
        scanf("%i", &temp);
        getchar();
        sum = sum + temp;
        product *= temp;       
    }
    
    printf("The sum is %i\n", sum);
    printf("The product is %i\n", product);
    getchar();
    
    return 0;
    
}