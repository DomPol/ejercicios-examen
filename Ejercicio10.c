#include <stdio.h>

//10. Write a program that asks for 2 angles of a triangle and find the third angle

int main ()
{
    float angle1;
    float angle2;
    
    printf ("Type two angles of a triangle\n\n");
    
    printf ("Angle 1:");
    scanf("%f", &angle1);
    getchar ();
    
    printf ("Angle 2:");
    scanf("%f", &angle2);
    getchar ();
    
    if (360 < angle1+angle2)
    { 
    system("cls");
    printf ("Error, the angles selected can`t make a triangle");
    system("color 40");
    getchar();
    }
    
    else if (0 >= angle1 || 0 >= angle2)
    { 
    system("cls");
    printf ("Error, the angles can't be negative or equal to 0");
    system("color 40");
    getchar();
    }
    
    else 
    {
    printf ("The third angle is %f", 360 - angle1 - angle2);
    getchar();
    }
        
    
    
    return 0;
}