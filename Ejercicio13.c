#include <stdio.h>

//13. Write a program that asks for a sequence of numbers to the user and show the sum of all of them. Process stops when a negative number is introduced

int main ()
{
    float temp;
    int sum = 0;
    int num = 1;
    
    printf("Type five positive numbers:");
    
    while (num <=5)
    {
        num++;
        scanf("%f", &temp);
        getchar();
        
        if (0 > temp)
        {
           system ("cls");
           system ("color 40");
           printf("I said positive numbers -_-");
           getchar();
           printf("Why do you have to be so negative ;C");
           getchar();
           
           return 0;           
        }
        
        else
        {
        sum = sum + temp;   
        }
            
             
    }
    
    printf("The sum is %i\n", sum);
    getchar();
    
    return 0;
}