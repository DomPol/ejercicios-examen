#include <stdio.h>  

//06. Write a program that asks for a value in centimeter and convert it into meter and kilometer

int main()
{
    float centimiter;
   
    printf("Type your value in centimeter\n");
    scanf("%f", &centimiter);
    getchar();
    
    system("cls");
   
    printf("%f cm is %f m and %f km\n", centimiter, centimiter/100, centimiter/100000);
    getchar();
        
   return 0;
}