#include <stdio.h>

//15. Write a program that asks for 1 number to the user and show its binary equivalent value. You can show the binary in inverted order

int main ()
{
    int num;
    int x= 0; 
    int counter =0;
    int res[100];
    
    printf("Type a number\n");
    scanf("%i", &num);
    getchar();
    
    system("cls");
    
    printf("%d in binary is : ", num);
    
    
    while (num != 1) 
    { 
    res[x] = num%2;
    num/=2; 
    x++;
    counter++;
    }
    
    for (x= 0; x < counter ; x++)
    {
    printf("%d", res[x]);
    }
     
    printf("1 (inverted)");
   
    getchar();
    
    
    return 0;
    
}