#include <stdio.h>

//17. Write a program that prints five random numbers, like a Lottery.

int main()
{
    time_t t;
    int x;
    int y = 5;
    
    printf("Welcome to the lotery\n");
    getchar();
    
    printf("Your lucky numbers are:");
    getchar();
    
    srand((unsigned) time(&t));
    
    for( x = 0 ; x < y ; x++ ) 
    {
      printf("%d, ", rand() % 100);
    }
    getchar();
    
    printf("\nToo bad, they are not the winning numbers");
    getchar();
    
    return 0;
    
}