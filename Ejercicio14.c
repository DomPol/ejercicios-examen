#include <stdio.h>

//14. Write a program that asks for a sequence of numbers to the user and show the mean of all of them. Process stops only when number 0 is introduced

int main ()
{
    //The counter starts at -1 because when the user presses 0 adds +1
    int counter = -1;
    float temp;
    float sum = 0;
    
    printf("Type as many numbers as you want, when you are done type 0\n");
    
    while (0 != temp)
    {
        scanf("%f", &temp);
        getchar();
        sum+= temp; 
        counter++;
    }
    
    printf("The mean of the numbers is %f", sum / counter);
    getchar();
    
    return 0;
    
}