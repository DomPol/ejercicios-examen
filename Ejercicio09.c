#include <stdio.h>

//09. Write a program that asks for seconds and convert it into minutes, hours and days

int main()
{
    float time;
    printf("Type seconds to convert into minutes, hours and days\n");
    scanf("%f", &time);
    getchar();
    
    system("cls");
    
    printf("%f seconds is:\n\n", time);
    printf("%f minutes\n", time/60);
    printf("%f hours\n", time/3600 );
    printf("%f days\n", time/86400);
    getchar();
    
    return 0;
}