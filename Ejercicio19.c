#include <stdio.h>

//19. Write a function to calculate power of a value. Function declaration should be: int RaiseToPower(int value, int power);

int RaiseToPower(int value, int power);

int main()
{
    int call;
    int powerMain;
    int valueMain;
      
    printf("Type a number");
    scanf("%i", &valueMain);
    getchar();
    
    printf("Type an exponent");
    scanf("%i", &powerMain);
    getchar();
    
    call = RaiseToPower(valueMain, powerMain);
    
    system ("cls");
    
    printf("%d raised to the power of %d is %d", valueMain, powerMain, call);
    getchar();
    
    return 0;
    
}

int RaiseToPower(int value, int power)
{
    
    int result= 1;    
    
    for (int x= 0; x < power; x++)
    {
        
        result *= value;
       
    }
    
    return result;    
}