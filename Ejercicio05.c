#include <stdio.h>
#include <math.h>


//05. Write a program that asks for radius of a circle and find its diameter, circumference and area

int main()
{
    float radius;
         
    printf("Type the radius of the circle in centimeters\n");
    scanf("%f", &radius);
    getchar();
    
    system("cls");
    
    printf("Your circle\n\n");
    printf("Diameter: %f cm\n", radius*2);
    printf("Circumference: %f cm\n", 3.14*radius*2);
    printf("Area: %f cm\n", pow(radius,2)*3.14);
    getchar();
    
    return 0;
}   