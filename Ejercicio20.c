#include <stdio.h>

/*20. Write a calculator program. Program flow should be as follow:
1) Ask for a number (float)
2) Ask for desired operation: '+', '-', '*', '/'
3) Ask for another number (float)
4) Show operation and results
5) Go to step 2)
*/
int main ()
{
    float num1;
    float num2;
    int operation;
    int x = 0;
    
    while (x == 0)
    {
        printf("Type a number");
        scanf("%f", &num1);
        getchar();
        
        system("cls");
        
        //operation selection
        printf("Desired operation\n\n");
        printf("1: Sum +\n");
        printf("2: Subtraction -\n");
        printf("3: Multiplication *\n");
        printf("4: Division /\n");
        
        scanf("%i", &operation);
        getchar();
        
        //Error prompt when a number other than 1,2,3,4 is typed
        while (operation != 1 && operation !=2 && operation !=3 && operation !=4)
        {
            printf("\nError, wrong number\n");
            printf("Try again, please\n");
            
            scanf("%i", &operation);
            getchar();
        }
        
        system("cls");
               
        printf("Type another number");
        scanf("%f", &num2);
        getchar();
        
        system("cls");
        
        //operation result
        if (operation == 1)
            {
                printf("%f + %f = %f", num1, num2, num1+num2);
            }
            
        else if (operation == 2)
            {
                printf("%f - %f = %f", num1, num2, num1-num2);
            }
        else if (operation == 3)
            {
                printf("%f * %f = %f", num1, num2, num1*num2);
            }
        else
            {
                printf("%f / %f = %f", num1, num2, num1/num2);
            }
        
        getchar();
            
        system("cls");
    }    
    
        
        
        return 0;
       
        
}